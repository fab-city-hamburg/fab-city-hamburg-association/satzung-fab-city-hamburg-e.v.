# Präambel und Satzung des Fab City Hamburg e.V. vom 20.10.2020

# Präambel

Mit der Digitalen Transformation geht einher, dass digitalisierte Information („Bits“) grundlegender Produktionsfaktor wird. Information, als ökonomisches Gut, ist immateriell und hat niedrige Grenzkosten der Produktion. Das heißt,sie ist leicht replizierbar und leicht global teilbar, also, im Gegensatz zu herkömmlichen grundlegenden Produktionsfaktoren, aus sich selbst heraus nicht knapp. Netzwerke, die quelloffene bits-basierte Lösungen produzieren, sind deswegen besonders leistungsstark. 

Im Angesicht ökologischer Herausforderungen des Anthropozäns und wachsender sozioökonomischer Disparitäten aufgrund zunehmend volatiler und akkumulierter Wertschöpfungsmustern, sind neue, auch institutionelle, Lösungsansätze notwendig.

Diese Lösungen quelloffen in Netzwerken zu produzieren und für Hamburg anzuwenden, ist das Ziel dieser Vereinigung.

Elementar für diese Vereinigung sind Orte, an denen digitalisierte Informationen,  in Wissen und physische Gegenstände umgesetzt werden. Dies sind offene Werkstätten mit digitaler Fertigung (Open Labs, oder auch Fab Labs). Hier werden, verbunden mit weltweiten Netzwerken, quelloffene Maschinen entwickelt, mit denen nahezu alles gefertigt werden kann. Somit entstehen im Rahmen dieser Vereinigung auch Circular Economy Labs, in denen verschiedene Stoffkreisläufe, wie etwa die von Plastik oder Kupfer, geschlossen werden (Kreislaufwirtschaft). Damit wird beabsichtigt die Schädigung des natürlichen Lebensraumes des Menschen zu verhindern. Zur Zielerreichung ist es auch notwendig, die Initiierung sich ökonomisch tragender Prozesse zu fördern, indem Räumlichkeiten zur Verfügung gestellt und Veranstaltungen durchgeführt werden. Zudem ist zur Zielerreichung, insbesondere der Realisierung einer Kreislaufwirtschaft, eine enge Zusammenarbeit mit der Kreativwirtschaft notwendig, damit Designer*innen und Kreative in sämtlichen Phasen von Entwicklungsprozessen Teil der Erarbeitung von Lösungen und innovativen Methoden sein können. Aufgrund des neuartigen und komplexen Charakters des Gegenstandes dieser Vereinigung, wird wissenschaftliche Begleitforschung betrieben.


# Satzung

## § 1 Name, Sitz, Eintragung, Geschäftsjahr


(1) Der Verein trägt den Namen Fab City Hamburg e.V.

(2) Er hat den Sitz in Hamburg.

(3) Er soll in das Vereinsregister eingetragen werden.

(4) Geschäftsjahr ist das Kalenderjahr.


## § 2 Vereinszweck

(1) Der Verein verfolgt ausschließlich und unmittelbar gemeinnützige Zwecke im Sinne des Abschnitts "Steuerbegünstigte Zwecke" der Abgabenordnung (§§ 51 ff.) in der jeweils gültigen Fassung. 

(2) Zweck des Vereins ist die Förderung von Wissenschaft und Forschung, die Förderung des Umweltschutzes, die Förderung der Volks- und Berufsbildung einschließlich der Studentenhilfe und die Förderung von Kunst und Kultur. 

(3) Der Satzungszweck wird insbesondere verwirklicht durch
Bereitstellen einer räumlichen, technischen und personellen Infrastruktur, die die Nutzenden anregt und befähigt, zum eigenen und gemeinschaftlichen Nutzen Kunst- und Designobjekte, Maschinen, Alltagsgegenstände sowie Mechanik-, Elektronik-, Hardware- und Software-Komponenten selbst zu entwerfen und herzustellen (Fab Lab beziehungsweise Open Lab). Teil dessen ist, dass Nutzende     niedrigschwellig befähigt werden, selbst Wert zu schöpfen und, im Sinne einer Kreislaufwirtschaft, aus „Abfällen“ Wertstoffe und neue Produkte zu produzieren.
     
Die Förderung von lokalen, global vernetzten, selbstorganisierten Prozessen des gemeinsamen bedürfnisorientierten und ressourcenschonenden Produzierens, Verwaltens, Pflegens und/oder     Nutzens von Gegenständen und sonstigen Ressourcen.
     
Entwicklung und Forschung im Bereich frei lizenzierter Produktionsmaschinen (Software und Hardware), sowie zu dem von der internationalen Fab City Foundation formulierten Ziel, bis zum Jahre 2054 die städtische Wirtschaft einer Fab City vollständig als Kreislaufwirtschaft umzugestalten, so dass keine physischen Güter oder Rohstoffe mehr im- oder exportiert werden, sondern alles, was eine Fab City konsumiert, lokal produziert wird. Die Vereinigung forscht selbst und in enger Zusammenarbeit mit Forschungseinrichtungen, insbesondere mit denen die Mitglied der Vereinigung sind. Forschungsergebnisse werden in Vereinseigenen Medien und Fachzeitschriften öffentlich publiziert.


Wissensvermittlung in den Bereichen: digitale Eigenproduktion, allgemeine Fertigungsverfahren inklusive der zugehörigen Werkstoffkunde, Selbstbau von Werkzeugmaschinen, Handwerkstechniken, neue Technologien, Computer und neue Medien, Kreislaufwirtschaft mittels digitaler Fertigungstechnologien, sowie den sonstigen unter Nr. 1 bis 3 beschriebenen Bereichen.
     
Veranstaltung von Schulungen und Workshops zur Aus- und Weiterbildung im Zusammenhang mit den unter. Nr. 1 bis 4 beschriebenen Bereichen.


Durchführung von Bildungsveranstaltungen und Workshops speziell für Kinder, Jugendliche und Schüler; Kooperationen mit Schulen, Bildungs- und Forschungseinrichtungen im Zusammenhang mit den unter. Nr. 1 bis 4 beschriebenen Bereichen.


Durchführung von sonstigen Projekten, Veranstaltung von Vorträgen, Seminaren und Tagungen im Zusammenhang mit den unter. Nr. 1 bis 4 beschriebenen Bereichen.


Die Entwicklung von Methoden zur Einbindung der Kreativwirtschaft in alle Phasen der Produktentwicklungsprozesskette, sowie intersektoralen Austausch auf Augenhöhe zwischen allen an dem Prozess beteiligten Gewerken. Hierzu gehört auch die Schaffung von Orten der Begegnung, und die Verankerung von Design-Prinzipien einer Kreislaufwirtschaft, wie etwa Modularität und Recyclebarkeit, in dezentrale und netzwerk-basierte Produktentstehungsprozesse.
Darüber hinaus gehört dazu die Einbindung künstlerischer Arbeiten zum Bereich Gesellschaft, Kultur, Design, Fertigungs- und Handwerkstechniken, Computer, neue Medien in das Vereinsleben unter anderem durch Ausstellungen künstlerischer Werke mit digitaler Fertigung in den Vereinsräumen.
(4) Der Satzungszweck wird auch verwirklicht durch Beschaffung und Zuwendung von Mitteln, sowie das zur Verfügung stellen von Arbeitskräften oder Räumen an steuerbegünstigte Körperschaften, die ebenfalls die in Absatz 2 genannten Zwecke verfolgen, im Sinne von § 58 Nr. 1, Nr. 2, Nr. 4 und Nr. 5 der Abgabenordnung.


## § 3 Selbstlosigkeit

(1) Der Verein ist selbstlos tätig; er verfolgt nicht in erster Linie eigenwirtschaftliche Zwecke.

(2) Mittel des Vereins dürfen nur für die satzungsmäßigen Zwecke verwendet werden. Die Mitglieder erhalten keine Zuwendungen aus Mitteln des Vereins.

(3) Es darf keine Person durch Ausgaben, die dem Zweck der Körperschaft fremd sind, oder durch unverhältnismäßig hohe Vergütungen begünstigt werden.


## § 4 Mitgliedschaft

(1) Die Mitgliedschaft des Vereins unterteilt sich in ordentliche und außerordentliche Mitglieder. Ordentliches Mitglied des Vereins kann jede juristische Person, Körperschaft des öffentlichen Rechts und Personengesellschaft werden, die seine Ziele unterstützt. Außerordentliches Mitglied des Vereins kann jede natürliche Person werden, die seine Ziele unterstützt. Insofern in dieser Satzung von Mitgliedern die Rede ist, sind sowohl ordentliche, als auch außerordentliche Mitglieder gemeint.

(2) Über den Antrag auf Aufnahme als Mitglied in den Verein entscheidet der Vorstand. Lehnt der Vorstand ein Mitgliedsantrag ab, ist dem Antragenden dies schriftlich zu begründen und die Antragenden haben die Möglichkeit die nächste Mitgliederversammlung anzurufen, um ihren Aufnahmewunsch trotz Ablehnung durch den Vorstand durchsetzen zu können.

(3) Die Mitgliedschaft endet durch Austritt, Ausschluss, Tod oder durch Erlöschen der juristischen Person oder Personengesellschaft.

(4) Der Austritt eines Mitgliedes ist nur zum Ende eines Monats möglich. Er erfolgt durch Erklärung in Textform gegenüber dem Vorsitzenden unter Einhaltung einer Frist von 14 Tagen.

(5) Wenn ein Mitglied gegen die Ziele und Interessen des Vereins schwer verstoßen hat oder trotz Mahnung mit dem Beitrag für sechs Monate im Rückstand bleibt, so kann es durch den Vorstand mit sofortiger Wirkung ausgeschlossen werden. Dem Mitglied muss vor der Beschlussfassung Gelegenheit zur Rechtfertigung bzw. Stellungnahme gegeben werden. Gegen den Ausschließungsbeschluss kann innerhalb einer Frist von 14 Tagen nach Mitteilung des Ausschlusses Berufung eingelegt werden. , über die die nächste Mitgliederversammlung entscheidet.

(6) Die Mitglieder können sich in dem Vorstand und der Mitgliederversammlung durch eine schriftlich bevollmächtigte Person vertreten lassen.


## § 5 Beiträge

Die Mitglieder zahlen Beiträge nach Maßgabe eines Beschlusses der Mitgliederversammlung. Zur Festlegung der Beitragshöhe und -fälligkeit ist eine einfache Mehrheit der in der Mitgliederversammlung anwesenden stimmberechtigten Vereinsmitglieder erforderlich.


## § 6 Organe des Vereins

Organe des Vereins sind

a) der Vorstand

b) die Mitgliederversammlung

c) der wissenschaftliche Beirat


## § 7 Der Vorstand

(1) Der Vorstand besteht aus fünf bis sieben Mitgliedern: einer oder einem Vorsitzenden, zwei stellvertretenden Vorsitzenden, einem Kassenwart und mindestens einer weiteren Person.

Der Vorstand vertritt den Verein gerichtlich und außergerichtlich. Der oder die Vorstandsvorsitzende ist gemeinsam mit einem weiteren Vorstandsmitglied vertretungsberechtigt. Bei Verhinderung kann der oder die Vorstandsvorsitzende von einem der stellvertretenden Vorsitzenden vertreten werden.

(2) Der Vorstand wird von der Mitgliederversammlung für die Dauer von einem Jahr gewählt. Die Wiederwahl der Vorstandsmitglieder ist möglich. Der oder die Vorsitzende und die stellvertretenden Vorsitzenden werden von der Mitgliederversammlung in einem besonderen Wahlgang bestimmt. Die jeweils amtierenden Vorstandsmitglieder bleiben nach Ablauf ihrer Amtszeit im Amt bis Nachfolger gewählt sind.

(3) Dem Vorstand obliegt die Führung der laufenden Geschäfte des Vereins. Er hat insbesondere folgende Aufgaben: Der Vorstand kann für die Geschäfte der laufenden Verwaltung einen Geschäftsführer bestellen. Dieser ist berechtigt, an den Sitzungen des Vorstandes mit beratender Stimme teilzunehmen. Der Vorstand wählt die Mitglieder des wissenschaftlichen Beirats.

(4) Vorstandssitzungen finden jährlich mindestens zweimal statt. Die Einladung zu Vorstandssitzungen erfolgt durch den Vorsitzenden in Textform.

(5) Der Vorstand fasst seine Beschlüsse mit einfacher Mehrheit der anwesenden Vorstandsmitglieder. Für eine Beschlussfähigkeit muss der Vorstandsvorsitzende sowie mindestens zwei weitere Vorstandsmitglieder anwesend sein.

(6) Beschlüsse des Vorstands können bei Eilbedürftigkeit auch in Textform oder fernmündlich gefasst werden, wenn mindestens zwei Drittel der Vorstandsmitglieder ihre Zustimmung zu diesem Verfahren in Textform oder fernmündlich erklären.

(7) Beschlüsse des Vorstandes sind zu protokollieren.

(8) Der Vorstand kann für seine Tätigkeit eine angemessene Vergütung erhalten, die Höhe der Vergütung wird von der Mitgliederversammlung festgesetzt. 

(9) Die Vorstandsmitglieder sind von den Beschränkungen des § 181 BGB befreit.


## § 8 Mitgliederversammlung

(1) Die Mitgliederversammlung ist einmal jährlich einzuberufen.

(2) Eine außerordentliche Mitgliederversammlung ist einzuberufen, wenn es das Vereinsinteresse erfordert oder wenn die Einberufung von einem Drittel der Vereinsmitglieder in Textform und unter Angabe des Zweckes und der Gründe verlangt wird. Zu diesem Drittel der Vereinsmitglieder zählen auch außerordentliche Vereinsmitglieder.

(3) Die Einberufung der Mitgliederversammlung erfolgt in Textform durch den Vorsitzenden unter Wahrung einer Einladungsfrist von mindestens 14 Tagen bei gleichzeitiger Bekanntgabe der Tagesordnung. Die Frist beginnt mit dem auf die Absendung des Einladungsschreibens folgenden Tag. Das Einladungsschreiben gilt dem Mitglied als zugegangen, wenn es an die letzte vom Mitglied des Vereins in Textform bekannt gegebene Adresse gerichtet ist.

(4) Die Mitgliederversammlung kann als Präsenzversammlung oder als virtuelle Mitgliederversammlung abgehalten werden. Zur Präsenzversammlung treffen sich alle Teilnehmer der Mitgliederversammlung an einem gemeinsamen Ort. Die virtuelle Mitgliederversammlung erfolgt durch Einwahl aller Teilnehmer in eine Video- oder Telefonkonferenz. Eine Kombination von Präsenzversammlung und virtueller Mitgliederversammlung ist möglich, indem den Mitgliedern die Möglichkeit eröffnet wird, an der Präsenzversammlung mittels Video-oder Telefonkonferenz teilzunehmen. Der Vorstand entscheidet über die Form der Mitgliederversammlung und teilt diese in der Einladung zur Mitgliederversammlung mit. Lädt der Vorstand zu einer virtuellen Mitgliederversammlung ein, so teilt er den Mitgliedern spätestens eine Stunde vor Beginn der Mitgliederversammlung per E-Mail die Einwahldaten für die Video-oder Telefonkonferenz mit.

(5) Die Mitgliederversammlung als das oberste beschlussfassende Vereinsorgan ist grundsätzlich für alle Aufgaben zuständig, sofern bestimmte Aufgaben gemäß dieser Satzung nicht einem anderen Vereinsorgan übertragen wurden. Ihr sind insbesondere die Jahresrechnung und der Jahresbericht zur Beschlussfassung über die Genehmigung und die Entlastung des Vorstandes schriftlich vorzulegen. Sie bestellt zwei Kassenprüfer, die weder dem Vorstand noch einem vom Vorstand berufenen Gremium angehören und auch nicht Angestellte des Vereins sein dürfen, um die Buchführung einschließlich Jahresabschluss zu prüfen und über das Ergebnis vor der Mitgliederversammlung zu berichten.

Die Mitgliederversammlung entscheidet auch über

a) Gebührenbefreiungen,

b) Aufgaben des Vereins,

c) An- und Verkauf sowie Belastung von Grundbesitz,

d) Beteiligung an Gesellschaften,

e) Aufnahme von Darlehen ab EUR 5000,-

f) Genehmigung aller Geschäftsordnungen für den Vereinsbereich,

g) Mitgliedsbeiträge,

h) Satzungsänderungen,

i) Auflösung des Vereins.

(6) Jede satzungsmäßig einberufene Mitgliederversammlung wird als beschlussfähig anerkannt ohne Rücksicht auf die Zahl der erschienenen Vereinsmitglieder. Sie wählt aus ihrer Mitte einen Versammlungsleiter. Ordentliche Mitglieder sind rede-, antrags- und stimmberechtigt. Außerordentliche Mitglieder sind rede- und antragsberechtigt, haben allerdings kein Stimmrecht.

(7) Die Mitgliederversammlung fasst ihre Beschlüsse mit einfacher Mehrheit der abgegebenen Stimmen. Bei Stimmengleichheit gilt ein Antrag als abgelehnt.

(8) Für Satzungsänderungen ist eine Mehrheit von zwei Dritteln der erschienenen stimmberechtigten Vereinsmitglieder erforderlich. Über Satzungsänderungen kann in der Mitgliederversammlung nur abgestimmt werden, wenn auf diesen Tagesordnungspunkt bereits in der Einladung zur Mitgliederversammlung hingewiesen wurde, mindestens ein Fünftel der ordentlichen Mitglieder erscheint, und der Einladung sowohl der bisherige als auch der vorgesehene neue Satzungstext beigefügt worden waren. Sind für eine Satzungsänderung nicht genügend ordentliche Mitglieder anwesend, ist der Vorstand berechtigt, eine zweite Versammlung, mit einer hinsichtlich der Satzungsänderung gleichen Tagesordnung, einzuberufen, die ohne Rücksicht auf die Zahl der anwesenden ordentlichen Mitglieder beschlussfähig ist. Darauf muss in der entsprechenden Einladung hingewiesen werden. Satzungsänderungen, die von Aufsichts- oder Finanzbehörden oder Gerichten aus formalen Gründen oder zur Wahrung der Gemeinnützigkeit verlangt werden, kann der Vorstand von sich aus vornehmen. Diese Satzungsänderungen müssen allen Vereinsmitgliedern alsbald in Textform mitgeteilt werden.

(9) Über die Beschlüsse der Mitgliederversammlung ist ein Protokoll anzufertigen, das vom Versammlungsleiter und dem Schriftführer zu unterzeichnen ist. Die Protokolle stehen den Mitgliedern zur Einsicht zur Verfügung.


## § 9 Wissenschaftlicher Beirat

(1) Zur Unterstützung des Vereins in wissenschaftlichen Fragen kann ein wissenschaftlicher Beirat gewählt werden, der nach Bedarf aus einer oder mehreren Personen bestehen kann.

(2) Die Mitglieder des wissenschaftlichen Beirates werden durch den Vorstand für die Dauer von zwei Jahren gewählt. Eine Wiederwahl ist möglich.

(3) Die Mitglieder des wissenschaftlichen Beirates müssen nicht Mitglieder des Vereins sein.

(4) Die Tätigkeit im wissenschaftlichen Beirat erfolgt ehrenamtlich. Die Mitglieder des wissenschaftlichen Beirats erhalten keine Vergütung oder sonstigen Zuwendungen aus Mitteln des Vereins. 


## § 10 Haftungsbegrenzung

(1) Organmitglieder oder besondere Vertreter haften dem Verein für einen bei der Wahrnehmung ihrer Pflichten verursachten Schaden nur bei Vorliegen von Vorsatz oder grober Fahrlässigkeit. Satz 1 gilt auch für die Haftung gegenüber den Mitgliedern des Vereins. Ist streitig, ob ein Organmitglied oder ein besonderer Vertreter einen Schaden vorsätzlich oder grob fahrlässig verursacht hat, trägt der Verein oder das Vereinsmitglied die Beweislast.

(2) Sind Organmitglieder oder besondere Vertreter nach Absatz 1 Satz 1 einem anderen zum Ersatz eines Schadens verpflichtet, den sie bei der Wahrnehmung ihrer Pflichten verursacht haben, so können sie von dem Verein die Befreiung von der Verbindlichkeit verlangen. Satz 1 gilt nicht, wenn der Schaden vorsätzlich oder grob fahrlässig verursacht wurde.


## § 11 Aufwandsersatz

(1) Mitglieder – soweit sie vom Vorstand beauftragt wurden – und Vorstandsmitglieder haben einen Anspruch auf Ersatz der Aufwendungen, die ihnen im Rahmen ihrer Tätigkeit für den Verein entstanden sind. Dazu gehören insbesondere Reisekosten, Verpflegungsmehraufwendungen, Porto und Kommunikationskosten.

(2) Der Nachweis erfolgt über entsprechende Einzelbelege und ist spätestens 6 Wochen nach Ende des jeweiligen Quartals geltend zu machen.

(3) Soweit für den Aufwandsersatz steuerliche Pauschalen und steuerfreie Höchstgrenzen bestehen, erfolgt ein Ersatz nur in dieser Höhe.


## § 12 Auflösung des Vereins und Vermögensbindung

(1) Für den Beschluss, den Verein aufzulösen, ist eine Zweidrittelmehrheit der in der Mitgliederversammlung anwesenden stimmberechtigten Mitglieder erforderlich. Der Beschluss kann nur nach rechtzeitiger Ankündigung in der Einladung zur Mitgliederversammlung gefasst werden.

(2) Bei Auflösung oder Aufhebung des Vereins oder bei Wegfall steuerbegünstigter Zwecke fällt das Vermögen des Vereins an eine juristische Person des öffentlichen Rechts oder eine andere steuerbegünstigte Körperschaft zwecks Verwendung für die Förderung von Wissenschaft und Forschung.


